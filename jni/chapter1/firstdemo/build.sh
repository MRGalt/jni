saveDir="build"

if [ ! -d "$saveDir" ]; then  
	mkdir "$saveDir"  
fi 
gcc -g -Wall -I $JAVA_HOME/include/ -I $JAVA_HOME/include/linux -fPIC -shared -o $saveDir/libfirstdemo.so firstdemo.c
mv $saveDir/libfirstdemo.so $HOME/lib/libfirstdemo.so
